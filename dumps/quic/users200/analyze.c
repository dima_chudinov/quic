#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pcap.h>


pcap_t *pd;
char errbuf[PCAP_ERRBUF_SIZE];
int total_packets = 0;
int total_length = 0;


void
countit( u_char *user, const struct pcap_pkthdr *h, const u_char *sp)
{
        total_length += h->len;
        total_packets++;
}
void
sig(int signo)
{
        printf("total len = %d, total packets = %d\n", total_length,
total_packets);
}

int
main(int argc, char *argv[])
{
        int count;
        int linktype;
        char *ifname;
        bpf_u_int32 localnet, netmask;


        (void)signal(SIGINT, sig);
        pd = pcap_open_offline(argv[1], errbuf);
        if (! pd) {
                puts(errbuf);
                exit(1);
        }
        linktype = pcap_datalink(pd);
        printf("linktype %s\n", pcap_datalink_val_to_name(linktype));

        localnet = 0;
        netmask = 0;
        count = pcap_loop(pd, -1, countit, 0);
        if ( count < 0)
                puts(pcap_geterr(pd));

        printf("total len = %d, total packets = %d\n", total_length,
total_packets);

        return 0;
}

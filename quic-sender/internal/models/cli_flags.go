package models

const (
	CliFlagHost     = "host"
	CliFlagPort     = "port"
	CliFlagDebug    = "debug"
	CliFlagInsecure = "insecure"
	CliFlagKey      = "key"
)

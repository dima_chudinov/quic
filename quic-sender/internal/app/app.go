package app

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/quic-go/quic-go/http3"
	"github.com/urfave/cli/v2"
	"gitlab.com/dima_chudinov/quic-sender/internal/models"
)

func RunQuicSender(cCtx *cli.Context) error {
	host := cCtx.String(models.CliFlagHost)
	port := cCtx.Int(models.CliFlagPort)
	addr := fmt.Sprintf("%s:%d", host, port)

	debug := cCtx.Bool(models.CliFlagDebug)

	insecure := cCtx.Bool(models.CliFlagInsecure)
	keyLogFile := ""

	var keyLog io.Writer
	if len(keyLogFile) > 0 {
		f, err := os.Create(keyLogFile)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		keyLog = f
	}

	pool, err := x509.SystemCertPool()
	if err != nil {
		log.Fatal(err)
	}

	roundTripper := &http3.RoundTripper{
		TLSClientConfig: &tls.Config{
			RootCAs:            pool,
			InsecureSkipVerify: insecure,
			KeyLogWriter:       keyLog,
		},
	}
	defer roundTripper.Close()
	hclient := &http.Client{
		Transport: roundTripper,
	}
	scanner := bufio.NewScanner(os.Stdin)

	for {
		scanner.Scan()
		// Holds the string that was scanned
		logString := scanner.Text()
		if len(logString) == 0 || logString == " " {
			continue
		}
		if debug {
			log.Printf("[quic-sender] sending to: %s; log string: %s\n", addr, logString)
		}
		rsp, err := hclient.Post(addr, "text/plain", strings.NewReader(logString))
		if err != nil {
			return err
		}
		body := &bytes.Buffer{}
		_, err = io.Copy(body, rsp.Body)
		if err != nil {
			return err
		}
		if debug {
			log.Printf("Response Body (%d bytes): %s\n", body.Len(), body.Bytes())
		}
	}

	return nil
}

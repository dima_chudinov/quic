package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/dima_chudinov/quic-sender/internal/app"
	"gitlab.com/dima_chudinov/quic-sender/internal/models"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     models.CliFlagHost,
				Usage:    "host for quic-receiver",
				EnvVars:  []string{"QUIC_RECEIVER_HOST"},
				Required: true,
			},
			&cli.IntFlag{
				Name:     models.CliFlagPort,
				Usage:    "port for quic-receiver",
				EnvVars:  []string{"QUIC_RECEIVER_PORT"},
				Required: true,
			},
			&cli.BoolFlag{
				Name:    models.CliFlagDebug,
				Usage:   "output address of the receiver server and log string",
				EnvVars: []string{"QUIC_SENDER_DEBUG"},
				Value:   false,
			},
			&cli.BoolFlag{
				Name:    models.CliFlagInsecure,
				Usage:   "skip certificate verification",
				EnvVars: []string{"QUIC_SENDER_INSECURE"},
				Value:   false,
			},
		},
		Action: app.RunQuicSender,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

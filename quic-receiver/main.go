package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/dima_chudinov/quic-sender/internal/app"
	"gitlab.com/dima_chudinov/quic-sender/internal/models"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:     models.CliFlagPort,
				Usage:    "port for quic-receiver",
				EnvVars:  []string{"QUIC_RECEIVER_PORT"},
				Required: true,
			},
			&cli.BoolFlag{
				Name:    models.CliFlagDebug,
				Usage:   "output address of the receiver server and log string",
				EnvVars: []string{"QUIC_RECEIVER_DEBUG"},
				Value:   false,
			},
			&cli.StringFlag{
				Name:     models.CliFlagCert,
				Usage:    "certificate public",
				EnvVars:  []string{"QUIC_RECEIVER_CERT"},
				Required: true,
			},
			&cli.StringFlag{
				Name:     models.CliFlagKey,
				Usage:    "certificate private key",
				EnvVars:  []string{"QUIC_RECEIVER_KEY"},
				Required: true,
			},
		},
		Action: app.RunQuicSender,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

package app

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"strconv"

	"github.com/quic-go/quic-go/http3"
	"github.com/urfave/cli/v2"
	"gitlab.com/dima_chudinov/quic-sender/internal/models"
)

func RunQuicSender(cCtx *cli.Context) error {
	// host := "0.0.0.0"
	port := cCtx.Int(models.CliFlagPort)
	// addr := fmt.Sprintf("%s:%d", host, port)

	debug := cCtx.Bool(models.CliFlagDebug)
	_ = debug

	// cert := cCtx.String(models.CliFlagCert)
	// key := cCtx.String(models.CliFlagKey)

	// handler := setupHandler()

	// server := http3.Server{
	// 	Handler: handler,
	// 	Addr:    addr,
	// 	QuicConfig: &quic.Config{
	// 		Tracer: qlog.DefaultTracer,
	// 	},
	// }
	// err := server.ListenAndServeTLS(cert, key)
	// if err != nil {
	// 	return err
	// }

	handler := setupHandler()
	address := "0.0.0.0:" + strconv.Itoa(port)
	server := http3.Server{
		Handler:   handler,
		Addr:      address,
		TLSConfig: generateTLSConfig(),
	}
	// certFile := "internal/resources/certificates/cert.crt"
	// keyFile := "internal/resources/certificates/key.key"
	err := server.ListenAndServe()
	if err != nil {
		return err
	}

	return nil
}

func setupHandler() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("error reading body while handling '/': %s\n", err.Error())
		}
		// log.Printf("Receive log: %s\n", string(body))
		fmt.Println(string(body))
	})

	return mux
}

func generateTLSConfig() *tls.Config {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	template := x509.Certificate{SerialNumber: big.NewInt(1)}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		panic(err)
	}
	keyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	certPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})

	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		panic(err)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
		NextProtos:   []string{"quic-echo-example"},
	}
}

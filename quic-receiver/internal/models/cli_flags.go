package models

const (
	CliFlagPort  = "port"
	CliFlagDebug = "debug"
	CliFlagKey   = "key"
	CliFlagCert  = "cert"
)
